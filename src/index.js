import React from 'react';
import ReactDOM from 'react-dom';
import 'normalize.css';
import 'animate.css';
import 'bootstrap/dist/css/bootstrap.css';
import './index.sass';
import App from './App';
import * as serviceWorker from './serviceWorker';
import { BrowserRouter as Router } from 'react-router-dom';
import { createStore, applyMiddleware } from 'redux';
import { Provider } from 'react-redux';
import logger from 'redux-logger';
import rootReducer from './redux/rootReducer';

export const store = createStore(rootReducer, applyMiddleware(logger));

const app = (
  <Provider store={store}>
    <Router>
      <App />
    </Router>
  </Provider>
);

console.log(store.getState());

ReactDOM.render(
  app,
  document.getElementById('root')
);

serviceWorker.unregister();
