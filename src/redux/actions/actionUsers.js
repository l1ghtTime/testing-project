import { USER_FORM_NAME_VALUE, USER_FORM_EMAIL_VALUE, USER_FORM_PHONE_VALUE, USER_FORM_LOAD_IMAGE, ADD_NEW_CARD, USER_FORM_WORK_POSITION, NAME_STATUS, EMAIL_STATUS, PHONE_STATUS, POSITION_STATUS, LOAD_STATUS, MODAL_STATUS, FORM_MARKER, BURGER_MENU_STATUS, } from "../actions/actionTypes";


export function addUserFormNameValue(payload) {
    return {
        type: USER_FORM_NAME_VALUE,
        payload: payload,
    }
}


export function addUserFormEmailValue(payload) {
    return {
        type: USER_FORM_EMAIL_VALUE,
        payload: payload,
    }
}

export function addUserFormPhoneValue(payload) {
    return {
        type: USER_FORM_PHONE_VALUE,
        payload: payload,
    }
}

export function addWorkPosition(payload) {
    return {
        type: USER_FORM_WORK_POSITION,
        payload: payload,
    }
}

export function addUserFormLoadImage(payload) {
    return {
        type: USER_FORM_LOAD_IMAGE,
        payload: payload,
    }
}

export function addNewCard(payload) {
    return {
        type: ADD_NEW_CARD,
        payload: payload,
    }
}

export function addNameStatus(payload) {
    return {
        type: NAME_STATUS,
        payload: payload,
    }
}

export function addEmailStatus(payload) {
    return {
        type: EMAIL_STATUS,
        payload: payload,
    }
}


export function addPhoneStatus(payload) {
    return {
        type: PHONE_STATUS,
        payload: payload,
    }
}


export function addPositionStatus(payload) {
    return {
        type: POSITION_STATUS,
        payload: payload,
    }
}

export function addLoadStatus(payload) {
    return {
        type: LOAD_STATUS,
        payload: payload,
    }
}


export function addFormMarker(payload) {
    return {
        type: FORM_MARKER,
        payload: payload,
    }
}

export function addModalStatus(payload) {
    return {
        type: MODAL_STATUS,
        payload: payload,
    }
}

export function addBurgerMenuStatus() {
    return {
        type: BURGER_MENU_STATUS,
    }
}


