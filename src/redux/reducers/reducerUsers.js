import {USER_FORM_NAME_VALUE, USER_FORM_EMAIL_VALUE, USER_FORM_PHONE_VALUE, USER_FORM_WORK_POSITION, USER_FORM_LOAD_IMAGE, ADD_NEW_CARD, NAME_STATUS, EMAIL_STATUS, PHONE_STATUS, POSITION_STATUS, LOAD_STATUS, MODAL_STATUS, FORM_MARKER, BURGER_MENU_STATUS, } from "../actions/actionTypes";

import person from '../../image/photo-cover.png'

const initialState = {
    users: [
        {
            img: person,
            title: 'Maximillian',
            description: 'Leading specialist of the Control Department',
            mail: 'controldepartment@gmail.com',
            phone: '+380 50 678 03 24'
        },

        {
            img: person,
            title: 'Adolph Blaine Charles David Earl Matthew Matthew',
            description: 'Contextual advertising specialist',
            mail: 'adolph.blainecharles@.com',
            phone: '+380 50 678 03 24'
        },

        {
            img: person,
            title: 'Elizabeth',
            description: 'Frontend Developer',
            mail: 'elisabet.front@gmail.com',
            phone: '+380 50 678 03 24'
        },

        {
            img: person,
            title: 'Alexander Jayden',
            description: 'Backend Developer',
            mail: 'alexander.back@gmail.com',
            phone: '+380 50 678 03 24'
        },

        {
            img: person,
            title: 'Noah',
            description: 'QA',
            mail: 'noah1998@gmail.com',
            phone: '+380 50 678 03 24'
        },

        {
            img: person,
            title: 'Liamgrievescasey Smith Wiam',
            description: 'Lead designer',
            mail: 'liamgrievescasey.smith@.com',
            phone: '+380 50 678 03 24'
        },
        
    ],

    userFormNameValue: '',
    userFormEmailValue: '',
    userFormPhoneValue: '',
    userFormWorkPosition: '',
    userFormLoadImagePath: '',
    nameStatus: null,
    emailStatus: null,
    phoneStatus: null,
    positionStatus: null,
    loadStatus: null,
    modalStatus: false,
    markerForm: false,
    burgerMenuStatus: false
}

const reducerUsers = (state = initialState, action) => {

    switch (action.type) {

        case USER_FORM_NAME_VALUE:
            return {
                ...state,
                userFormNameValue: action.payload
            }

        case USER_FORM_EMAIL_VALUE:
            return {
                ...state,
                userFormEmailValue: action.payload
            }

        case USER_FORM_PHONE_VALUE:
            return {
                ...state,
                userFormPhoneValue: action.payload
        }

        case USER_FORM_WORK_POSITION:
            return {
                ...state,
                userFormWorkPosition: action.payload
        }

        case USER_FORM_LOAD_IMAGE:
            return {
                ...state,
                userFormLoadImagePath: action.payload
        }

        case ADD_NEW_CARD:
            return {
                ...state,
                users: state.users.concat(action.payload)
        }


        case NAME_STATUS:
            return {
                ...state,
                nameStatus: action.payload
        }

        case EMAIL_STATUS:
            return {
                ...state,
                emailStatus: action.payload
        }

        case PHONE_STATUS:
            return {
                ...state,
                phoneStatus: action.payload
        }


        case POSITION_STATUS:
            return {
                ...state,
                positionStatus: action.payload
        }

        case LOAD_STATUS:
            return {
                ...state,
                loadStatus: action.payload
        }

        case FORM_MARKER:
            return {
                ...state,
                markerForm: action.payload
        }

        case MODAL_STATUS:
            return {
                ...state,
                modalStatus: action.payload
        }

        case BURGER_MENU_STATUS:
            return {
                ...state,
                burgerMenuStatus: !state.burgerMenuStatus
        }
            


        default:
            return state;
    }

};

export default reducerUsers;