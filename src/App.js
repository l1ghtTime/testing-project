import React from 'react';
import Header from './Components/Header/Header';
import "./App.sass"
import Main from './Components/Main/Main';
import Footer from './Components/Footer/Footer';
import Modal from './Components/Modal/Modal';

function App() {
  return (
    <div className="App">
      <Header />
      <Main />
      <Footer />
      <Modal />
    </div>
  );
}

export default App;
