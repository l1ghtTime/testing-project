import React, { useEffect, useState } from 'react';
import "../Main/Main.sass";
import banner from '../../image/banner-photo.jpg';
import manLaptop from '../../image/man-laptop-v1.png'
import { Link } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';
import { addUserFormNameValue, addUserFormEmailValue, addUserFormPhoneValue, addWorkPosition, addUserFormLoadImage, addNewCard, addNameStatus, addEmailStatus, addPhoneStatus, addPositionStatus, addLoadStatus, addModalStatus, addFormMarker, } from '../../redux/actions/actionUsers';

const Main = () => {
    const state = useSelector(state => state.reducerUsers);
    const [FirstVal, setFirstVal] = useState(0);
    const [SeconsVal, setSeconsVal] = useState(6);
    const [step, setStep] = useState(0);
    const [oldElems] = useState([]);
    const [visualUsers, setVisualUsers] = useState([]);
    let interval = state.users.slice(`${FirstVal}`, `${SeconsVal}`);

    useEffect(() => {
        oldElems.unshift(interval);
        let combineArr = oldElems.flat();
        setVisualUsers(combineArr);

    }, [step]);
    function handleClick() {
        setStep(step + 1);
        setFirstVal(FirstVal + 6);
        setSeconsVal(SeconsVal + 6);
    }
    // JS FOR FORM
    const dispatch = useDispatch();
    function handleChangeName(event) {
        dispatch(addUserFormNameValue(event.target.value));
        event.target.value.length > 0 ? dispatch(addNameStatus(true)) : dispatch(addNameStatus(false))
    }
    function handleChangeEmail(event) {
        dispatch(addUserFormEmailValue(event.target.value));

        if (event.target.value.length > 0) {
            dispatch(addEmailStatus(true));
        }
    }
    function handleChangePhone(event) {
        dispatch(addUserFormPhoneValue(event.target.value));

        if (event.target.value.length > 0) {
            dispatch(addPhoneStatus(true));
        }
    }
    function handleActionFront(event) {
        const parent = event.target.parentNode;
        dispatch(addWorkPosition(parent.children[1].innerHTML));
        dispatch(addPositionStatus(true));
    }
    function handleActionBack(event) {
        const parent = event.target.parentNode;
        dispatch(addWorkPosition(parent.children[1].innerHTML));
        dispatch(addPositionStatus(true));
    }
    function handleActionDesigner(event) {
        const parent = event.target.parentNode;
        dispatch(addWorkPosition(parent.children[1].innerHTML));
        dispatch(addPositionStatus(true));
    }
    function handleActionQa(event) {
        const parent = event.target.parentNode;
        dispatch(addWorkPosition(parent.children[1].innerHTML));
        dispatch(addPositionStatus(true));
    }
    function handleLoadFile(event) {
        let url = URL.createObjectURL(event.target.files[0]);

        dispatch(addUserFormLoadImage(url));

        if (url.length > 0) {
            dispatch(addLoadStatus(true));
        }
    }
    function handleBtnClick(event) {
        const inputAreas = [state.userFormNameValue, state.userFormEmailValue, state.userFormPhoneValue, state.userFormWorkPosition, state.userFormLoadImagePath];
        const inputActionStatus = [addNameStatus, addEmailStatus, addPhoneStatus, addPositionStatus, addLoadStatus];

        inputAreas.map(item => {
            return inputActionStatus.map(actionStatus => {
                return item.length < 1 ? dispatch(actionStatus(false)) : dispatch(actionStatus(true));
            }) 
        });
        if (
            state.nameStatus === true &&
            state.emailStatus === true &&
            state.phoneStatus === true &&
            state.positionStatus === true &&
            state.loadStatus === true
        ) {
            dispatch(addNewCard(
                {
                    img: state.userFormLoadImagePath,
                    title: state.userFormNameValue,
                    description: state.userFormWorkPosition,
                    mail: state.userFormEmailValue,
                    phone: state.userFormPhoneValue
                }
            ));
            dispatch(addFormMarker(true));
            dispatch(addModalStatus(true));
        }
    }
    useEffect(() => {
        if (state.userFormLoadImagePath.length > 0) {
            dispatch(addLoadStatus(true));
        }
    }, []);

    return (
        <React.Fragment>
            {/* Section with BANNER*/}
            <div className="banner" style={{ background: `url(${banner})` }}>
                <div className="row">
                    <div className="col-12 col-sm-12 col-lg-7">
                        <div className="card-text card-text--test">
                            <h2 className="card-text__title">TEST ASSIGNMENT FOR FRONTEND DEVELOPER POSITION</h2>
                            <p className="card-text__description">
                                We kindly remind you that your test assignment should be submitted
                                as a link to github/bltbucket repository. <span className="card-text__hide">Please be patient, we consider
                                and respond to every application that meets minimum requirements.
                                We look forward to your submission. Good luck! The photo has to scale
                                In the banner area on the different screens
                                </span>
                            </p>
                            <Link to="/" className="card-text__btn btn btn--red">Sing up now</Link>
                        </div>
                    </div>
                </div>
            </div>
            {/* Section with CARD-LAPTOP */}
            <h2 className="title title--laptop">Let's get acquainted</h2>
            <div className="card card--laptop">
                <div className="row">
                    <div className="col-12 col-sm-5 col-lg-5">
                        <img src={manLaptop} className="card__img" alt="men-laptop" />
                    </div>
                    <div className="col-12 col-sm-7 col-lg-7">
                        <div className="card__info">
                            <h4 className="card__title">I am cool frontend developer</h4>
                            <p className="card__description">
                                We will evaluate how clean your approach to writing CSS and Javascript code is.
                                You can use any CSS and Javascript 3rd party libraries without any restriction.
                            </p>
                            <p className="card__description">
                                If 3rd party css/javascript libraries are added to the project via
                                bower/npm/yarn you will get bonus points. If you use any task runner
                                (gulp/webpack) you will get bonus points as well. Slice service directory
                                page PSD mockup into HTML5/CSS3.
                            </p>
                            <Link to="/" className="card__link">Sing up now</Link>
                        </div>
                    </div>
                </div>
            </div>
            {/* Section USERS */}
            <div className="users">
                <h2 className="title title--users">Our cheerful users</h2>
                <h5 className="subtitle">Attention! Sorting users by registration date</h5>
                <div className="users__box">
                    <div className="row">
                        {
                            visualUsers.map((card, index) => {
                                let mails = card.mail.split('@');
                                return (
                                    <div className="col-sm-4 offset-sm-0 col-lg-3 offset-lg-1" key={index}>
                                        <div className="card card--user">
                                            <img src={card.img} className="card__img" alt="person-icon" />
                                            <h6 className={card.title.length > 15 && card.title.length < 20 ? 'card__title card__title--limit' : 'card__title'}>{card.title}</h6>
                                            <p className="card__description">{card.description}</p>
                                            <span className="card__mail">
                                                {card.mail.length > 24 ? `${mails[0]}@...` : card.mail}
                                                {card.mail.length > 24
                                                    ? <span className="card__message">{card.mail}</span>
                                                    : null
                                                }
                                            </span>
                                            <span className="card__phone">{card.phone}</span>
                                        </div>
                                    </div>);
                            })
                        }
                    </div>
                    {state.users.length > oldElems.flat().length ?
                        <Link to="/" className="users__btn btn btn--red" onClick={handleClick}>Show more</Link>
                        : null
                    }
                </div>
            </div>
            {/* Section FORM */}
            <div className="user-form">
                <h2 className="title user-form__title">Register to get a work</h2>
                <h5 className="subtitle user-form__subtitle">Attention! After successful registration and alert, update the list of users in the block from the top</h5>
                <form className="form user-form__form">
                    <div className="form-column">
                        <div className="form__group form-group w-100">
                            <label htmlFor="inputUserForm" className="form__label" style={state.nameStatus === false ? { color: '#db3445' } : { color: 'inherit' }}>Name</label>
                            <input type="text" className="form__area form-control" id="inputUserForm" placeholder="Your name" autoComplete="off"  style={state.nameStatus === false ? { border: '1px solid #db3445' } : null} required onChange={handleChangeName} />
                            <span className="user-form__message">{state.nameStatus === false ? 'You should write some value!' : ''}</span>
                        </div>
                        <div className="form__group form-group w-100">
                            <label htmlFor="inputEmailUserForm" className="form__label" style={state.emailStatus === false ? { color: '#db3445' } : { color: 'inherit' }}>Email</label>
                            <input type="email" className="form__area form-control" id="inputEmailUserForm" placeholder="Your email" autoComplete="off" style={state.emailStatus === false ? { border: '1px solid #db3445' } : null} required onChange={handleChangeEmail} />
                            <span className="user-form__message">{state.emailStatus === false ? 'You should write some value!' : ''}</span>
                        </div>
                        <div className="form__group form-group w-100">
                            <label htmlFor="inputPhoneUserFrom" className="form__label" style={state.phoneStatus === false ? { color: '#db3445' } : { color: 'inherit' }}>Phone number</label>
                            <input type="email" className="form__area form-control" id="inputPhoneUserFrom" placeholder="+380 XX XXX XX XX" autoComplete="off" style={state.phoneStatus === false ? { border: '1px solid #db3445' } : null} required onChange={handleChangePhone} />
                            <span className="user-form__message">{state.phoneStatus === false ? 'You should write some value!' : ''}</span>
                        </div>
                        <div className="form__group form-group w-100">
                            <div className="form__position" style={state.positionStatus === false ? { color: '#db3445' } : { color: 'inherit' }}>Select Your position</div>
                            <div className="form__custom-control custom-control custom-radio form__radio">
                                <input type="radio" className="custom-control-input" id="frontend-checkbox" name="radio-stacked" required onChange={handleActionFront} />
                                <label className="custom-control-label form__label" htmlFor="frontend-checkbox">Frontend developer</label>
                            </div>
                            <div className="form__custom-control custom-control custom-radio form__radio">
                                <input type="radio" className="custom-control-input" id="backend-checkbox" name="radio-stacked" required onChange={handleActionBack} />
                                <label className="custom-control-label form__label" htmlFor="backend-checkbox">Backend developer</label>
                            </div>
                            <div className="form__custom-control custom-control custom-radio form__radio">
                                <input type="radio" className="custom-control-input" id="designer-checkbox" name="radio-stacked" required onChange={handleActionDesigner} />
                                <label className="custom-control-label form__label" htmlFor="designer-checkbox">Designer</label>
                            </div>
                            <div className="form__custom-control custom-control custom-radio form__radio">
                                <input type="radio" className="custom-control-input" id="qa-checkbox" name="radio-stacked" required onChange={handleActionQa} />
                                <label className="custom-control-label form__label" htmlFor="qa-checkbox">QA</label>
                            </div>
                            <span className="user-form__message">{state.positionStatus === false ? 'You should to choose your position!' : ''}</span>
                        </div>
                        <div className="custom-file user-form__custom-file">
                            <label htmlFor="validatedCustomFile" className="form__label user-form__custom-lable" style={state.loadStatus === false ? { color: '#db3445' } : { color: 'inherit' }}>Photo</label>
                            <input type="file" className="custom-file-input" id="validatedCustomFile" required onChange={handleLoadFile} />
                            <label className="custom-file-label user-form__download-lable" htmlFor="validatedCustomFile" style={state.loadStatus === false ? { border: '1px solid #db3445' } : null}>Upload your photo</label>
                            <div className="invalid-feedback">Example invalid custom file feedback</div>
                            <span className="user-form__message">{state.loadStatus === false ? 'You should load some photo!' : ''}</span>
                        </div>
                        <Link to="/" className="btn btn--red user-form__btn" onClick={handleBtnClick}>Sing up now</Link>
                    </div>
                </form>
            </div>

        </React.Fragment>
    )
}
export default Main;
