import React, { useState } from 'react';
import { Link } from 'react-router-dom';
import "../Header/Header.sass";
import logo from '../../image/favicon-32x32.png';
import burgerMenu from '../../image/menu-icon.png';
import { addBurgerMenuStatus } from '../../redux/actions/actionUsers';
import { useDispatch, useSelector } from 'react-redux';


const Header = () => {
    const burgerStatus = useSelector(state => state.reducerUsers);
    const dispatch = useDispatch();
    const [state, setstate] = useState('inherit')
    window.addEventListener('scroll', (event) => {
        window.pageYOffset > 55 ? setstate('fixed') : setstate('inherit');
    });

    function handleClick(event) {
        dispatch(addBurgerMenuStatus());
    }

    return ( 
        <React.Fragment>
            {state === 'fixed' ?
                <div className="fake-header"></div>
                : null
            }
            <div className="header"  style={{position: `${state}`}}>
                <div className="container-md">
                    <div className="row">
                        <div className="col-10 col-sm-3 col-md-2 col-lg-2 col-xl-2">
                            <Link to="/" className="header__logo logo">
                                <img src={logo} className="logo__img" alt="logo"/>
                                <h1 className="logo__title">TESTTASK</h1>
                            </Link>
                        </div>
                        <div className="col-2 offset-0 col-sm-1 offset-sm-8 col-md-8 offset-md-2 col-lg-6 offset-lg-4 col-xl-6 offset-xl-4">
                            <nav className="header__menu navigation-menu">
                                <ul className="navigation-menu__list">
                                    <li className="navigation-menu__item">
                                        <Link to="/" className="navigation-menu__link">About me</Link>
                                    </li>
                                    <li className="navigation-menu__item">
                                        <Link to="/" className="navigation-menu__link">Relationships</Link>
                                    </li>
                                    <li className="navigation-menu__item">
                                        <Link to="/" className="navigation-menu__link">Requirements</Link>
                                    </li>
                                    <li className="navigation-menu__item">
                                        <Link to="/" className="navigation-menu__link">Users</Link>
                                    </li>
                                    <li className="navigation-menu__item">
                                        <Link to="/" className="navigation-menu__link">Sign Up</Link>
                                    </li>

                                </ul>
                            </nav>
                            <Link to="/" className="burger" onClick={handleClick}>
                                <img src={burgerMenu} className="burger__img" alt="burger-menu"/>
                            </Link>
                        </div>
                    </div>
                </div>

                {burgerStatus.burgerMenuStatus === true ?
                        <nav className="burger-menu animate__animated animate__backInLeft">
                            <ul className="burger-menu__list">
                                <li className="burger-menu__item">
                                    <Link to="/" className="header__logo logo">
                                        <img src={logo} className="logo__img" alt="logo"/>
                                        <h1 className="logo__title">TESTTASK</h1>
                                    </Link>
                                </li>
                                <hr/>
                                <li className="burger-menu__item">
                                    <Link to="/" className="burger-menu__link">About me</Link>
                                </li>
                                <li className="burger-menu__item">
                                    <Link to="/" className="burger-menu__link">Relationships</Link>
                                </li>
                                <li className="burger-menu__item">
                                    <Link to="/" className="burger-menu__link">Requirements</Link>
                                </li>
                                <li className="burger-menu__item">
                                    <Link to="/" className="burger-menu__link">Users</Link>
                                </li>
                                <li className="burger-menu__item">
                                    <Link to="/" className="burger-menu__link">Sign Up</Link>
                                </li>
                                <hr/>

                                <li className="burger-menu__item">
                                    <Link to="/" className="burger-menu__link">How it work</Link>
                                </li>
                                <li className="burger-menu__item">
                                    <Link to="/" className="burger-menu__link">Partnership</Link>
                                </li>
                                <li className="burger-menu__item">
                                    <Link to="/" className="burger-menu__link">Help</Link>
                                </li>
                                <li className="burger-menu__item">
                                    <Link to="/" className="burger-menu__link">Leave testimonial</Link>
                                </li>
                                <li className="burger-menu__item">
                                    <Link to="/" className="burger-menu__link">Contact us</Link>
                                </li>
                                <hr/>

                                <li className="burger-menu__item">
                                    <Link to="/" className="burger-menu__link">Articles</Link>
                                </li>
                                <li className="burger-menu__item">
                                    <Link to="/" className="burger-menu__link">Our news</Link>
                                </li>
                                <li className="burger-menu__item">
                                    <Link to="/" className="burger-menu__link">Testimonials</Link>
                                </li>
                                <li className="burger-menu__item">
                                    <Link to="/" className="burger-menu__link">Licenses</Link>
                                </li>
                                <li className="burger-menu__item">
                                    <Link to="/" className="burger-menu__link">Privacy Policy</Link>
                                </li>
                                <hr/>
                            </ul>
                        </nav>
                    : null
                }

            </div>
        </React.Fragment>
    )
}
export default Header;
