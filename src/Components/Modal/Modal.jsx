import React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { addModalStatus } from '../../redux/actions/actionUsers';
import "../Modal/Modal.sass";

const Modal = () => {
    const state = useSelector(state => state.reducerUsers);
    const dispatch = useDispatch();

    function handleClickBtn(event) {
        dispatch(addModalStatus(false))
    }  
    function handleClickClose(event) {
        dispatch(addModalStatus(false))
    } 
    return (
        <React.Fragment>
            {state.modalStatus === true ? 
                <div className="modal" tabIndex="-1" role="dialog" style={state.modalStatus === true ? {display: "block"} : null}>
                    <div className="modal-dialog" role="document">
                        <div className="modal-content">
                            <div className="modal-header">
                                <h5 className="modal-title">Congradulation</h5>
                                <button type="button" className="close" data-dismiss="modal" aria-label="Close" onClick={handleClickClose}>
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div className="modal-body">
                                <p>You have successfully passed the registration</p>
                            </div>
                            <div className="modal-footer">
                                <button type="button" className="btn modal__btn" onClick={handleClickBtn}>Great</button>
                            </div>
                        </div>
                    </div>
                </div>
                : null 
            }
        </React.Fragment>

    )
}
export default Modal;
